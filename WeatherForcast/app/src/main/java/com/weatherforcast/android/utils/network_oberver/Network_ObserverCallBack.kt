package com.lanparty.app.utils.network_oberver

interface Network_ObserverCallBack {

    fun onNetWorkDisconnected()
    fun onNetWorkConnected()
}