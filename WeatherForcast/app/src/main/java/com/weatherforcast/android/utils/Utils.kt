package com.weatherforcast.android.utils

val CLOUDS="Clouds"
val CLEAR="Clear"
val CLEAR_DAY="ClearDay"
val THUNDER="Thunderstorm"
val DRIZZLE="Drizzle"
val RAIN="Rain"
val SNOW="Snow"


val MIST_="Mist"
val SMOKE_="Smoke"
val HAZE_="Haze"
val DUST_="Dust"
val FOG_="Fog"
val SAND_="sand"
val ASH_="Ash"
val SQAULL_="squall"
val TORNADO="Tornado"
