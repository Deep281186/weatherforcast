package com.weatherforcast.android.home.fragments.ui.forcast

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.instacart.library.truetime.TrueTime
import com.lanparty.app.utils.error_models.ApiErrors
import com.lanparty.app.utils.error_models.ApiErrors_withInternalCode
import com.lanparty.app.utils.error_models.ApiException
import com.lanparty.app.utils.error_models.NoInternetException
import com.weatherforcast.android.errorHandler.CONNECTION_ERROR
import com.weatherforcast.android.errorHandler.HOSTNOTFOUND
import com.weatherforcast.android.errorHandler.SOMETHINGWRONG
import com.weatherforcast.android.home.adapter.ForcastAdapter
import com.weatherforcast.android.home.adapter.ForcastSubAdapter
import com.weatherforcast.android.home.fragments.ui.forcast.dataModel.FilteredDataModel
import com.weatherforcast.android.home.fragments.ui.forcast.dataModel.Weatherdata
import com.weatherforcast.android.utils.CLEAR
import com.weatherforcast.android.webUtils.Coroutines
import com.weatherforcast.android.webUtils.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ForcastViewModel(val repository: Repository) : ViewModel() {

    var showProgress = MutableLiveData<Boolean>()
    var filteredData = MutableLiveData<ArrayList<FilteredDataModel>>()
    var errorText = MutableLiveData<String>()
    var errorFound = MutableLiveData<Boolean>()

    var forcastAdapter: ForcastAdapter? = null


    init {
        forcastAdapter = ForcastAdapter()
    }


    fun getforcastAdapter(): ForcastAdapter? {
        return forcastAdapter
    }

    fun updateForcastAdapter(dataList: ArrayList<FilteredDataModel>) {
        forcastAdapter?.updateAdapter(dataList)
    }

    fun getForcast(q: String) {
        var error_message = "0"
        showProgress.value = true
        errorFound.value = false
        var filteredDataList = ArrayList<FilteredDataModel>()
        var hourlyForcastList = ArrayList<Weatherdata>()
        Coroutines.networkJob_onBg_thread {
            try {
                val reponse = repository.getForcast(q)

                reponse?.let { responseData ->

                    var timeZone = responseData.city.timezone * 1000

                    responseData.list?.let { dataList ->
                        //making list according to days
                        dataList.asIterable().forEachIndexed { index, x ->
                            var UTCMillis = localToUTC(x.dt * 1000)

                            if (index == 0) {
                                filteredDataList.add(
                                    FilteredDataModel(
                                        getDateFromMilli(UTCMillis + timeZone),
                                        "",
                                        "",
                                        "",
                                        "", "",
                                        "", "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        reponse.city.name,
                                        hourlyForcastList

                                    )
                                )
                            } else {
                                var flag = false
                                filteredDataList.asIterable()
                                    .forEachIndexed { index, filteredDataModel ->
                                        if (filteredDataModel.day.equals(
                                                getDateFromMilli(UTCMillis + timeZone),
                                                true
                                            )
                                        ) {
                                            flag = true
                                        }

                                    }
                                if (!flag) {
                                    filteredDataList.add(
                                        FilteredDataModel(
                                            getDateFromMilli(UTCMillis + timeZone),
                                            "",
                                            "",
                                            "",
                                            "", "",
                                            "", "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            reponse.city.name,
                                            hourlyForcastList

                                        )
                                    )
                                }


                            }

                        }
                        var SunSetUTCMIllis = localToUTC(responseData.city.sunset * 1000)
                        var SunRiseUTCMillis = localToUTC(responseData.city.sunrise * 1000)
                        //filtering data from response on the basis of day
                        filteredDataList.asIterable()
                            .forEachIndexed { filteredata_index, filteredDataModel ->
                                var hourlyForcastList = ArrayList<Weatherdata>()
                                var maxTemp = ArrayList<Double>()
                                var minTem = ArrayList<Double>()
                                var maxHumidity = ArrayList<Double>()
                                var maxWind = ArrayList<Double>()
                                dataList.asIterable().forEach { dataListModel ->
                                    var UTCMillis = localToUTC(dataListModel.dt * 1000)
                                    var timeMillis = UTCMillis + timeZone

                                    if (filteredDataModel.day.equals(
                                            getDateFromMilli(UTCMillis + timeZone)
                                        )
                                    ) {

                                        hourlyForcastList.add(
                                            Weatherdata(
                                                Math.round(dataListModel.main.temp).toString() + "\u2103",
                                                "Min: " + Math.round(dataListModel.main.tempMin).toString() + "\u2103",
                                                "Max: " + Math.round(dataListModel.main.tempMax).toString() + "\u2103",
                                                getTimeFromMilli(UTCMillis + timeZone),
                                                "http://openweathermap.org/img/wn/" + dataListModel.weather.get(
                                                    0
                                                ).icon + "@2x.png",
                                                dataListModel.weather.get(0).main
                                                )
                                        )
                                        maxTemp.add(dataListModel.main.tempMax)
                                        minTem.add(dataListModel.main.tempMin)
                                        maxHumidity.add(dataListModel.main.humidity)
                                        maxWind.add(dataListModel.wind.speed)
                                    }
                                }

                                filteredDataModel.max =
                                    Math.round(Collections.max(maxTemp)).toString() + "\u2103"
                                filteredDataModel.min =
                                    Math.round(Collections.min(minTem)).toString() + "\u2103"
                                filteredDataModel.maxMin =
                                    filteredDataModel.max + "/" + filteredDataModel.min
                                filteredDataModel.humidity =
                                    Math.round(Collections.max(maxHumidity)).toString() + "%"
                                filteredDataModel.wind =
                                    Math.round(Collections.max(maxWind)).toString() + " m/sec"
                                filteredDataModel.desc = hourlyForcastList.get(0).desc
                                if (hourlyForcastList.get(0).desc.equals(CLEAR)) {
                                    filteredDataModel.main = hourlyForcastList.get(0).desc + "Day"
                                } else {
                                    filteredDataModel.main = hourlyForcastList.get(0).desc
                                }

                                filteredDataModel.icon =
                                    "http://openweathermap.org/img/wn/" + hourlyForcastList.get(0).icon + "@2x.png"
                                filteredDataModel.current_temp =
                                    Math.round(Collections.max(maxTemp)).toString() + "\u2103"
                                filteredDataModel.hourlyForcast = hourlyForcastList

                            }

                    }


                }
                return@networkJob_onBg_thread
            } catch (ex: ApiErrors) {
                error_message = ex.error_message
            } catch (ex: ApiErrors_withInternalCode) {
                error_message = ex.error_message
            } catch (e: ApiException) {

                error_message = SOMETHINGWRONG
            } catch (e: NoInternetException) {
                error_message = CONNECTION_ERROR
            } catch (ex: UnknownHostException) {
                error_message = HOSTNOTFOUND
            } finally {
                withContext(Dispatchers.Main) {
                    showProgress.value = false
                    if (!error_message.equals("0", true)) {
                        errorText.value = error_message
                        errorFound.value = true

                    } else {

                        filteredData.value = filteredDataList
                    }
                }
            }
        }
    }

    companion object {

        @JvmStatic
        @BindingAdapter("setForcastAdapter")
        fun setForcastAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>) {
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter
        }

        @JvmStatic
        @BindingAdapter("setForcastSubAdapter")
        fun setForcastSubAdapter(
            recyclerView: RecyclerView,

            dataList: ArrayList<Weatherdata>
        ) {
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = ForcastSubAdapter(dataList)
        }
    }


    private fun localToUTC(time: Long): Long {
        try {
            val dateFormat = SimpleDateFormat("yyyy.MM.dd HH:mm:ss")
            val date = Date(time)
            dateFormat.timeZone = TimeZone.getTimeZone("UTC")
            val strDate = dateFormat.format(date)
            val dateFormatLocal = SimpleDateFormat("yyyy.MM.dd HH:mm:ss")
            val utcDate = dateFormatLocal.parse(strDate)
            return utcDate.time
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return time
    }


    private fun getDateFromMilli(millis: Long): String {
        val formatter = SimpleDateFormat("EEEE,MMM dd")

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        return formatter.format(calendar.time)
    }

    private fun getTimeFromMilli(millis: Long): String {
        val formatter = SimpleDateFormat("hh a")

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        return formatter.format(calendar.time)
    }


}