package com.lanparty.app.utils.error_models
import com.google.gson.annotations.SerializedName

data class Error_Model_1(
    @SerializedName("cod")
    val cod: String,
    @SerializedName("message")
    val message: String
)