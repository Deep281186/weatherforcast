package com.weatherforcast.android.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.weatherforcast.android.BR
import com.weatherforcast.android.R
import com.weatherforcast.android.databinding.RcvItemBinding
import com.weatherforcast.android.home.fragments.ui.forcast.ForcastViewModel
import com.weatherforcast.android.home.fragments.ui.forcast.dataModel.FilteredDataModel

class ForcastAdapter() :
    RecyclerView.Adapter<ForcastAdapter.ItemViewHolder>() {


    var dataList = ArrayList<FilteredDataModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            DataBindingUtil.inflate<RcvItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.rcv_item,
                parent,
                false
            )

        )

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {

        holder.bind(dataList.get(position))
    }

    class ItemViewHolder : RecyclerView.ViewHolder {
        var itemViewHolder: ViewDataBinding? = null

        constructor(
            binding: RcvItemBinding

        ) : super(binding.root) {
            this.itemViewHolder = binding
        }

        fun bind(filteredDataModel: FilteredDataModel) {
            itemViewHolder?.setVariable(BR.data, filteredDataModel)
            itemViewHolder?.executePendingBindings()

        }
    }

    fun updateAdapter(dataList: ArrayList<FilteredDataModel>) {

        if (this.dataList.size > 0) {
            this.dataList.clear()
            this.dataList.addAll(dataList)
            notifyDataSetChanged()
        } else {
            this.dataList.addAll(dataList)
            notifyDataSetChanged()
        }


    }


}