package com.weatherforcast.android.errorHandler


const val CONNECTION_ERROR="Connection error!"
const val HOSTNOTFOUND="Host not found!"
const val SOMETHINGWRONG="Something went wrong."
const val SUCCESS_200=200
const val ERROR_400=400
const val ERROR_401=401
const val ERROR_404=404
const val EXP_2000=2000
const val EXP_DEF_CODE=5000


