package com.lanparty.app.utils.error_models

import java.io.IOException
import java.io.InterruptedIOException
import java.net.UnknownHostException

class ApiException(message: String):IOException(message)
class ApiErrors(title:String,message: String,code:Int):IOException(message){
  var  error_message=message
  var title=title


}
class ApiErrors_withInternalCode(message: String,code:Int,internalCode_:Int):IOException(message){
  var  error_code:Int=code
  var  error_message=message

}
class NoInternetException(message: String):IOException(message)
