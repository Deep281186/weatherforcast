package com.weatherforcast.android.home.fragments.ui.forcast

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.sportsparty.app.factory.ViewModel_factory
import com.weatherforcast.android.R
import com.weatherforcast.android.databinding.FragmentForcastBinding
import com.weatherforcast.android.errorHandler.CONNECTION_ERROR
import com.weatherforcast.android.home.NewHomeActivity
import com.weatherforcast.android.home.fragments.ui.forcast.dataModel.FilteredDataModel
import com.weatherforcast.android.home.fragments.ui.home.dataModel.ActivityCordinator
import com.weatherforcast.android.utils.progressDialog
import kotlinx.android.synthetic.main.activity_new_home.*
import kotlinx.android.synthetic.main.fragment_forcast.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ForcastFragment : Fragment(), KodeinAware, ActivityCordinator {
    override fun initiateNetWorkCall(name: String) {
        forcastViewModel?.getForcast(name)
    }

    var errorObserver = Observer<Boolean> {
        it?.let { error ->
            if (error) {

                if (forcastViewModel?.errorText?.value.toString().equals(CONNECTION_ERROR)) {
                    errorLayoutHandler(true, true)
                } else {
                    errorLayoutHandler(false, true)
                }
            } else {
                errorLayoutHandler(false, false)
            }

        }
    }
    var filteredDataList = Observer<ArrayList<FilteredDataModel>> { dataList ->

        dataList?.let {
            if (it.size > 0) {
                forcastViewModel?.updateForcastAdapter(it)
            }
        }
    }

    var showProgress = Observer<Boolean> {
        it?.let { showProgress ->

            if (showProgress) {
                errorLayoutHandler(false, false)

            } else {
                (context_ as NewHomeActivity).setTag(0)
            }
            progressVisibilityHandle(showProgress)
        }

    }
    private var forcastViewModel: ForcastViewModel? = null
    override val kodein by kodein()
    var context_: Context? = null
    private val factory: ViewModel_factory by instance()
    var progressDialog: Dialog? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var forcastBinding: FragmentForcastBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_forcast, container, false)
        forcastViewModel =
            ViewModelProviders.of(this@ForcastFragment, factory).get(ForcastViewModel::class.java)
        forcastBinding.viewModel = forcastViewModel
        forcastBinding.lifecycleOwner = viewLifecycleOwner
        return forcastBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.onCreate(savedInstanceState)
        forcastViewModel?.showProgress?.observe(this, showProgress)
        forcastViewModel?.errorFound?.observe(this, errorObserver)
        forcastViewModel?.filteredData?.observe(this, filteredDataList)
        progressDialog = context_?.progressDialog()
        var name = (context_ as NewHomeActivity).et_cityName.text.toString()
        img_icon_forcast.visibility=View.GONE
        if (name.isNullOrBlank()) {
            ll_errorHandler_forcast?.visibility = View.VISIBLE
            forcastViewModel?.errorText?.value = "Search city name."
           // errorLayoutHandler(false, true)
        } else {
            forcastViewModel?.getForcast(name)
        }
        (context_ as NewHomeActivity).registerCordinator(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context_ = context
    }

    private fun progressVisibilityHandle(showProgress: Boolean) {
        progressDialog?.let {
            if (showProgress) {
                progressDialog?.show()
            } else {
                progressDialog?.dismiss()
            }
        }
    }

    private fun errorLayoutHandler(connectionError: Boolean, showLayout: Boolean) {

        if (showLayout) {
            ll_errorHandler_forcast?.visibility = View.VISIBLE
            img_icon_forcast.visibility=View.VISIBLE
            if (connectionError) {
                img_icon_forcast.setImageResource(R.drawable.no_internet)
            } else {
                img_icon_forcast.setImageResource(R.drawable.ic_error_outline_black_24dp)
            }
        } else {
            img_icon_forcast?.visibility = View.GONE
            ll_errorHandler_forcast?.visibility = View.GONE
        }

    }
}