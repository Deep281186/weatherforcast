package com.weatherforcast.android.home.fragments.ui.forcast.dataModel

data class FilteredDataModel(
    var day: String,
    var icon: String,
    var current_temp: String,
    var min: String,
    var max: String,
    var maxMin: String,
    var desc: String,
    var main: String,
    var humidity: String,
    var wind: String,
    var sunrise: String,
    var sunset: String,
    var cityName: String,
    var hourlyForcast: ArrayList<Weatherdata>
)

data class Weatherdata(
    var current_temp: String,
    var min: String,
    var max: String,
    var time: String,
    var icon: String,
    var desc: String
)