package com.weatherforcast.android.home

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import com.lanparty.app.utils.network_oberver.Network_ObserverCallBack
import com.lanparty.app.utils.network_oberver.NewNetwork_Observer
import com.weatherforcast.android.R
import com.weatherforcast.android.home.fragments.ui.forcast.ForcastFragment
import com.weatherforcast.android.home.fragments.ui.home.HomeFragment
import com.weatherforcast.android.home.fragments.ui.home.dataModel.ActivityCordinator
import com.weatherforcast.android.utils.hideKeyboard
import kotlinx.android.synthetic.main.activity_new_home.*
import kotlinx.android.synthetic.main.activity_new_home.view.*

class NewHomeActivity : AppCompatActivity(), Network_ObserverCallBack {
    var activityCordinator: ActivityCordinator? = null
    var ISCONNECTED = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_home)
        NewNetwork_Observer(this).registerMonitoring(this)
        setFragment()
    }


    fun registerCordinator(activityCordinator: ActivityCordinator) {
        this.activityCordinator = activityCordinator
    }

    fun getWeatherData(view: View) {
        hideKeyboard(view)
        if (!et_cityName.text.toString().isNullOrBlank()) {
            if (ISCONNECTED) {
                if (btn_getData.tag.toString().toInt() == 0) {
                    activityCordinator?.initiateNetWorkCall(et_cityName.text.toString())
                    btn_getData.tag = 1
                }
            } else {
                btn_getData.btn_getData.tag = 0
                Toast.makeText(this, "Network error", Toast.LENGTH_SHORT).show()
            }
        }


    }

    fun setTag(tag: Int) {
        btn_getData.tag = tag
    }

    override fun onNetWorkConnected() {
        ISCONNECTED = true
    }

    override fun onNetWorkDisconnected() {
        ISCONNECTED = false
    }

    fun replaceHomeFragment(view: View) {

        txt_today.setBackgroundResource(R.drawable.bottombar_bg)
        txt_5day.setBackgroundResource(0)
        var fragment = HomeFragment()
        supportFragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit()


    }

    fun replaceForcastFragment(view: View) {

        txt_today.setBackgroundResource(0)
        txt_5day.setBackgroundResource(R.drawable.bottombar_bg)
        var fragment = ForcastFragment()
        supportFragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit()

    }

    private fun setFragment() {
        var fragment = HomeFragment()
        supportFragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit()
    }

}
