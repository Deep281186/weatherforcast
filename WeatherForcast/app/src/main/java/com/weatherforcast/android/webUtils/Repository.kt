package com.weatherforcast.android.webUtils


import com.sportsparty.app.web_utils.SafeApiRequest
import com.weatherforcast.android.BuildConfig
import com.weatherforcast.android.home.fragments.ui.forcast.dataModel.ResponseModel
import com.weatherforcast.android.home.fragments.ui.home.dataModel.CurrentWeather_ResponseModel

class Repository(
    private val networkConnectionInterceptor: NetworkConnectionInterceptor


) : SafeApiRequest() {
    suspend fun getForcast(q: String): ResponseModel? {
        return apiRequest {
            ServiceGenerator.creatService(
                networkConnectionInterceptor
            ).getForeCast(q, BuildConfig.APIKEYS,"metric")
        }

    }

    suspend fun getCurrentWeather(q: String): CurrentWeather_ResponseModel? {
        return apiRequest {
            ServiceGenerator.creatService(
                networkConnectionInterceptor
            ).getCurrentWeather(q, BuildConfig.APIKEYS,"metric")
        }

    }
}