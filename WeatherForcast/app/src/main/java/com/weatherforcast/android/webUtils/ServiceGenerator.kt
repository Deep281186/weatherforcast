package com.weatherforcast.android.webUtils

import android.os.Build
import  com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.lanparty.app.utils.error_models.NoInternetException
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ServiceGenerator {

    fun creatService(
        networkConnectionInterceptor: NetworkConnectionInterceptor

    ): RequestApi {

        var httpClient = OkHttpClient.Builder()
        var builder = Retrofit.Builder().baseUrl(BASEURL)

            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder: Request.Builder
            requestBuilder = original.newBuilder().header("Accept", "application/json")
                .method(original.method(), original.body());
            val request = requestBuilder.build()
            return@addInterceptor chain.proceed(request)


        }
        httpClient.addInterceptor(networkConnectionInterceptor)
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        var client = httpClient
            .addInterceptor(interceptor) .connectTimeout(10, TimeUnit.SECONDS)
            .callTimeout(10, TimeUnit.SECONDS).build()

        val retrofit = builder.client(client).build()
        return retrofit.create(RequestApi::class.java)
    }





}