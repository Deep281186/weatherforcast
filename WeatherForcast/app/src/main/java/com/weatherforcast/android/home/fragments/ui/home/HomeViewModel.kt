package com.weatherforcast.android.home.fragments.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.instacart.library.truetime.TrueTime
import com.lanparty.app.utils.error_models.ApiErrors
import com.lanparty.app.utils.error_models.ApiException
import com.lanparty.app.utils.error_models.NoInternetException
import com.weatherforcast.android.errorHandler.CONNECTION_ERROR
import com.weatherforcast.android.errorHandler.HOSTNOTFOUND
import com.weatherforcast.android.errorHandler.SOMETHINGWRONG
import com.weatherforcast.android.home.fragments.ui.home.dataModel.FilteredData
import com.weatherforcast.android.webUtils.Coroutines
import com.weatherforcast.android.webUtils.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.lanparty.app.utils.error_models.ApiErrors_withInternalCode
import com.weatherforcast.android.R
import com.weatherforcast.android.utils.*


class HomeViewModel(val repository: Repository) : ViewModel() {


    var city = MutableLiveData<String>()
    var desc = MutableLiveData<String>()
    var icon = MutableLiveData<String>()
    var temp = MutableLiveData<String>()
    var main = MutableLiveData<String>()

    var feelLike = MutableLiveData<String>()
    var minMax = MutableLiveData<String>()
    var humidity = MutableLiveData<String>()
    var wind = MutableLiveData<String>()
    var day = MutableLiveData<String>()
    var sunsetrise = MutableLiveData<String>()
    var showProgress = MutableLiveData<Boolean>()
    var onSuccess = MutableLiveData<Boolean>()
    var errorText = MutableLiveData<String>()
    var errorFound = MutableLiveData<Boolean>()


    fun getCurrentWeather(q: String) {
        var error_message = "0"
        errorFound.value = false
        showProgress.value = true
        onSuccess.value = false
        var filteredData: FilteredData? = null
        Coroutines.networkJob_onBg_thread {
            try {
                val reponse = repository.getCurrentWeather(q)

                reponse?.let { responseData ->
                    var currentUTCMillis = localToUTC(getCurrentMillis())
                    var timeZone = responseData.timezone * 1000
                    var SunSetUTCMIllis = localToUTC(responseData.sys.sunset * 1000)
                    var SunRiseUTCMillis = localToUTC(responseData.sys.sunrise * 1000)
                    var dayOrNight = ""

                    if (((currentUTCMillis + timeZone) > (SunRiseUTCMillis + timeZone)&&(currentUTCMillis + timeZone) < (SunSetUTCMIllis + timeZone))&&responseData.weather.get(0).main.equals(
                            CLEAR)) {
                        dayOrNight = "Day"
                    }

                    filteredData = FilteredData(
                        responseData.id,
                        responseData.name,

                        responseData.weather.get(0).description,
                        Math.round(responseData.main.temp).toString() + "\u2103",
                        "http://openweathermap.org/img/wn/" + responseData.weather.get(0).icon + "@2x.png",
                        Math.round(responseData.main.feelsLike).toString() + "\u2103",
                        Math.round(responseData.main.tempMin).toString() + "\u2103",
                        Math.round(responseData.main.tempMax).toString() + "\u2103",
                        Math.round(responseData.main.humidity).toString() + "%",
                        getDateFromMilli(currentUTCMillis + timeZone),
                        Math.round(responseData.wind.speed).toString() + " meter/sec",
                        getTimeFromMilli(SunSetUTCMIllis + timeZone),
                        getTimeFromMilli(SunRiseUTCMillis + timeZone),
                        responseData.weather.get(0).main + dayOrNight


                    )

                }
                return@networkJob_onBg_thread
            } catch (ex: ApiErrors) {
                error_message = ex.error_message
            } catch (e: ApiException) {
                error_message = SOMETHINGWRONG
            } catch (e: NoInternetException) {
                error_message = CONNECTION_ERROR
            } catch (ex: UnknownHostException) {
                error_message = HOSTNOTFOUND
            } finally {
                withContext(Dispatchers.Main) {
                    showProgress.value = false

                    if (!error_message.equals("0", true)) {
                        errorText.value = error_message
                        errorFound.value = true
                        clearData()
                    } else {

                        onSuccess.value = true
                        bindData(filteredData)
                    }
                }
            }
        }
    }

    private fun bindData(filteredData: FilteredData?) {

        filteredData?.let { data ->

            city.value = data.cityName
            desc.value = data.desc
            icon.value = data.icon
            main.value = data.main
            temp.value = data.currentTemp
            feelLike.value = data.feelsLike
            minMax.value = data.minTemp + "/" + data.maxTemp
            humidity.value = data.humidity
            wind.value = data.wind
            day.value = data.day
            sunsetrise.value = data.sunrise + "/" + data.sunset

        }

    }

    private fun clearData() {
        city.value = ""
        desc.value = ""
        icon.value = "0"
        main.value = "0"
        temp.value = ""
        feelLike.value = ""
        minMax.value = ""
        humidity.value = ""
        wind.value = ""
        sunsetrise.value = ""
        day.value = ""
    }


    private fun getCurrentMillis(): Long {
        var currentLocal = Date()
        if (TrueTime.isInitialized()) {
            currentLocal = TrueTime.now()
        }

        return currentLocal.time
    }


    private fun localToUTC(time: Long): Long {
        try {
            val dateFormat = SimpleDateFormat("yyyy.MM.dd HH:mm:ss")
            val date = Date(time)
            dateFormat.timeZone = TimeZone.getTimeZone("UTC")
            val strDate = dateFormat.format(date)
            val dateFormatLocal = SimpleDateFormat("yyyy.MM.dd HH:mm:ss")
            val utcDate = dateFormatLocal.parse(strDate)
            return utcDate.time
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return time
    }


    private fun getDateFromMilli(millis: Long): String {
        val formatter = SimpleDateFormat("EEEE,MMM dd hh:mm a")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        return formatter.format(calendar.time)
    }

    private fun getTimeFromMilli(millis: Long): String {
        val formatter = SimpleDateFormat("hh:mm a")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        return formatter.format(calendar.time)
    }

    companion object {
        @JvmStatic
        @BindingAdapter("loadIcon")
        fun loadIcon(imageView: ImageView, desc: String?) {
            Log.e("Desc-->", "/" + desc)
            desc?.let {
                if (desc.equals("0")) {
                    imageView.setImageResource(0)

                } else {
                    when (desc) {
                        CLEAR_DAY -> {
                            imageView.setImageResource(R.drawable.yellow_circle)
                        }
                        CLEAR -> {
                            imageView.setImageResource(R.drawable.clearsky)
                        }
                        THUNDER -> {
                            imageView.setImageResource(R.drawable.thunder)
                        }
                        DRIZZLE -> {
                            imageView.setImageResource(R.drawable.drizzle)
                        }
                        RAIN -> {
                            imageView.setImageResource(R.drawable.rain)
                        }
                        SNOW -> {
                            imageView.setImageResource(R.drawable.snow)
                        }
                        CLOUDS -> {
                            imageView.setImageResource(R.drawable.ic_cloud)
                        }
                        TORNADO -> {
                            imageView.setImageResource(R.drawable.tornado)
                        }


                        else -> {
                            imageView.setImageResource(R.drawable.fog)
                        }

                    }

                }

            }

        }
    }


}