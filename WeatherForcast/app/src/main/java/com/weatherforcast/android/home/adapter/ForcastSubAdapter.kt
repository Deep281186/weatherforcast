package com.weatherforcast.android.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.weatherforcast.android.BR
import com.weatherforcast.android.R
import com.weatherforcast.android.databinding.ForcastsubitemBinding
import com.weatherforcast.android.home.fragments.ui.forcast.dataModel.Weatherdata

class ForcastSubAdapter(var dataList: ArrayList<Weatherdata>) :
    RecyclerView.Adapter<ForcastSubAdapter.ItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            DataBindingUtil.inflate<ForcastsubitemBinding>(
                LayoutInflater.from(
                    parent.context
                ), R.layout.forcastsubitem, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {

        holder.bind(dataList.get(position))
    }


    class ItemViewHolder : RecyclerView.ViewHolder {

        var itemViewHolder: ViewDataBinding? = null

        constructor(forcastsubitemBinding: ForcastsubitemBinding) : super(forcastsubitemBinding.root) {
            this.itemViewHolder = forcastsubitemBinding
        }

        fun bind(weatherdata: Weatherdata) {
            itemViewHolder?.setVariable(BR.data, weatherdata)
            itemViewHolder?.executePendingBindings()
        }

    }


}