package com.lanparty.app.utils.network_oberver

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.content.Context.CONNECTIVITY_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import kotlinx.coroutines.delay


class NewNetwork_Observer(private val networkObservercallback: Network_ObserverCallBack) :
    ConnectivityManager.NetworkCallback() {


    var networkAvail = false
    fun registerMonitoring(context: Context) {
        var netWorkRequest =
            NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build()
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.registerNetworkCallback(netWorkRequest, this)
    }

    override fun onAvailable(network: Network?) {
        super.onAvailable(network)
        networkObservercallback.onNetWorkConnected()



    }

    override fun onLost(network: Network?) {
        super.onLost(network)
        networkAvail = false
        networkObservercallback.onNetWorkDisconnected()
    }

    override fun onLosing(network: Network?, maxMsToLive: Int) {
        super.onLosing(network, maxMsToLive)
    }

    override fun onUnavailable() {
        super.onUnavailable()
        networkAvail = false
        networkObservercallback.onNetWorkDisconnected()
    }
}