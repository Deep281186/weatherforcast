package com.weatherforcast.android.webUtils

import android.content.Context
import android.net.ConnectivityManager
import com.lanparty.app.utils.error_models.NoInternetException
import com.weatherforcast.android.errorHandler.CONNECTION_ERROR

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class NetworkConnectionInterceptor(val context: Context) : Interceptor {

    private val applicationContext = context.applicationContext


    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isInternetAvailable()) {
            throw NoInternetException(CONNECTION_ERROR)
            // return null
        } else {
            try {
                return chain.proceed(chain.request())
            } catch (ex: IOException) {

                throw NoInternetException(CONNECTION_ERROR)

            }
        }
    }

    init {
        // Log.e("NetworkConnection-->","yes")
        //  FirebaseApp.initializeApp(context)
    }

    private fun isInternetAvailable(): Boolean {

        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.activeNetworkInfo.also {
            return it != null && it.isConnected
        }
    }
}