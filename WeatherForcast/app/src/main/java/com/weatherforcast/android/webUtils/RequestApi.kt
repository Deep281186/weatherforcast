package com.weatherforcast.android.webUtils


import com.weatherforcast.android.home.fragments.ui.forcast.dataModel.ResponseModel
import com.weatherforcast.android.home.fragments.ui.home.dataModel.CurrentWeather_ResponseModel
import retrofit2.Response
import retrofit2.http.*


interface RequestApi {


    @GET(GET_FORCAST)
    suspend fun getForeCast(
        @Query("q") q: String, @Query("appid") appid: String?,@Query("units") units: String?
    ): Response<ResponseModel>

    @GET(GET_CURRENTWEATHER)
    suspend fun getCurrentWeather(
        @Query("q") q: String, @Query("appid") appid: String?,@Query("units") units: String?
    ): Response<CurrentWeather_ResponseModel>



}