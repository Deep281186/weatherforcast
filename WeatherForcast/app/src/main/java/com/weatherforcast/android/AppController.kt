package com.weatherforcast.android

import android.app.Application
import android.content.Intent
import com.instacart.library.truetime.TrueTimeRx
import com.sportsparty.app.factory.ViewModel_factory
import com.weatherforcast.android.webUtils.NetworkConnectionInterceptor
import com.weatherforcast.android.webUtils.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.exceptions.Exceptions
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import java.util.*
import java.util.concurrent.atomic.AtomicReference

class AppController : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@AppController))
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { Repository(instance()) }
        bind() from provider {
            ViewModel_factory(instance())
        }
    }

    override fun onCreate() {
        super.onCreate()
        initRxTrueTime()
    }


    private fun initRxTrueTime() {
        val throwableRef = AtomicReference<Throwable>()
        RxJavaPlugins.setErrorHandler { throwable ->
            if (!throwableRef.compareAndSet(null, throwable)) {
                throw Exceptions.propagate(throwable)
            }
        }
        TrueTimeRx.build()
            .withConnectionTimeout(10000)
            .withRetryCount(3)
            .withLoggingEnabled(true)
            .initializeRx("time.google.com")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<Date>() {
                override fun onSuccess(date: Date) {
                    //  Log.e("TrueTime", "Success initialized TrueTime :" + date.toString());
                    sendBroadcast(Intent().putExtra("trueTime_status", true))
                }

                override fun onError(e: Throwable) {
                    //   Log.e("TrueTime", "something went wrong when trying to initializeRx TrueTime", e);
                    sendBroadcast(Intent().putExtra("trueTime_status", false))
                }
            })


    }
}