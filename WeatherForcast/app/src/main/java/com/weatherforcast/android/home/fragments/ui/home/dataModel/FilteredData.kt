package com.weatherforcast.android.home.fragments.ui.home.dataModel

data class FilteredData(
    var id: Int,
    var cityName: String,
    var desc: String,
    var currentTemp:String,
    var icon: String,
    var feelsLike: String,
    var minTemp: String,
    var maxTemp: String,
    var humidity: String,
    var day: String,
    var wind: String,
    var sunset: String,
    var sunrise: String,
    var main:String
)