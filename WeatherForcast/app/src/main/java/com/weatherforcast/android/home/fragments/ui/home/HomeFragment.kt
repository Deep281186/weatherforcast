package com.weatherforcast.android.home.fragments.ui.home

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.sportsparty.app.factory.ViewModel_factory
import com.weatherforcast.android.R
import com.weatherforcast.android.databinding.FragmentHomeBinding
import com.weatherforcast.android.errorHandler.CONNECTION_ERROR
import com.weatherforcast.android.home.NewHomeActivity
import com.weatherforcast.android.home.fragments.ui.home.dataModel.ActivityCordinator
import com.weatherforcast.android.utils.progressDialog
import kotlinx.android.synthetic.main.activity_new_home.*
import kotlinx.android.synthetic.main.fragment_forcast.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class HomeFragment : Fragment(), KodeinAware, ActivityCordinator {
    override fun initiateNetWorkCall(name: String) {
        homeViewModel.getCurrentWeather(name)
    }

    var errorObserver = Observer<Boolean> {
        it?.let { error ->
            if (error) {
                if (homeViewModel.errorText.value.toString().equals(CONNECTION_ERROR)) {
                    errorLayoutHandler(true, true)
                } else {
                    errorLayoutHandler(false, true)
                }
            } else {
                errorLayoutHandler(false, false)
            }

        }
    }
    var onSuccessObserver = Observer<Boolean> {
        it?.let { onSuccess ->
            if (onSuccess) {
                showDetailsLayout()
            } else {
                hideDeatilsLayout()
            }
        }
    }
    var showProgress = Observer<Boolean> {
        it?.let { showProgress ->

            if (showProgress) {
                errorLayoutHandler(false, false)
                hideDeatilsLayout()
            } else {
                (context_ as NewHomeActivity).setTag(0)
            }
            progressVisibilityHandle(showProgress)
        }

    }

    override val kodein by kodein()
    private lateinit var homeViewModel: HomeViewModel
    var context_: Context? = null
    private val factory: ViewModel_factory by instance()
    var progressDialog: Dialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var homeBinding: FragmentHomeBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        homeViewModel =
            ViewModelProviders.of(this@HomeFragment, factory).get(HomeViewModel::class.java)
        homeBinding.viewModel = homeViewModel
        homeBinding.lifecycleOwner = viewLifecycleOwner
        return homeBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.errorFound.observe(this, errorObserver)
        homeViewModel.onSuccess.observe(this, onSuccessObserver)
        homeViewModel.showProgress.observe(this, showProgress)
        progressDialog = context_?.progressDialog()
        var name = (context_ as NewHomeActivity).et_cityName.text.toString()
        img_icon.visibility=View.GONE
        if (name.isNullOrBlank()) {
            ll_errorHandler.visibility = View.VISIBLE
            homeViewModel.errorText.value = "Search city name."

        }else{
            homeViewModel.getCurrentWeather(name)
        }
        (context_ as NewHomeActivity).registerCordinator(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context_ = context
    }

    private fun progressVisibilityHandle(showProgress: Boolean) {
        progressDialog?.let {
            if (showProgress) {
                progressDialog?.show()
            } else {
                progressDialog?.dismiss()
            }
        }
    }


    private fun showDetailsLayout() {
        ll_details.animate().alpha(1f).setStartDelay(200).setDuration(200).start()
    }

    private fun hideDeatilsLayout() {
        ll_details.alpha = 0f
    }

    private fun errorLayoutHandler(connectionError: Boolean, showLayout: Boolean) {

        if (showLayout) {
            ll_errorHandler.visibility = View.VISIBLE
            img_icon.visibility=View.VISIBLE
            if (connectionError) {
                img_icon.setImageResource(R.drawable.no_internet)
            } else {
                img_icon.setImageResource(R.drawable.ic_error_outline_black_24dp)
            }
        } else {
            img_icon.visibility = View.GONE
            ll_errorHandler.visibility = View.GONE
        }

    }
}