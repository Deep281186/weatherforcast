package com.weatherforcast.android.webUtils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.broadcast
import kotlinx.coroutines.launch

object Coroutines {


    fun networkJob_onBg_thread(call: suspend (() -> Unit)) =
        CoroutineScope(Dispatchers.IO).launch {
            call()
        }



    fun onMainthread(call: suspend (() -> Unit)) =
    CoroutineScope(Dispatchers.Main).launch {
        call()
    }


}