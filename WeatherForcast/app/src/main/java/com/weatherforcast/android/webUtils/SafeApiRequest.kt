package com.sportsparty.app.web_utils

import android.util.Log
import com.google.gson.Gson
import com.lanparty.app.utils.error_models.*
import com.weatherforcast.android.errorHandler.CONNECTION_ERROR
import com.weatherforcast.android.errorHandler.EXP_2000
import com.weatherforcast.android.errorHandler.EXP_DEF_CODE
import com.weatherforcast.android.errorHandler.SOMETHINGWRONG


import retrofit2.Response
import java.lang.Exception

abstract class SafeApiRequest {

    suspend fun <T : Any> apiRequest(call: suspend () -> Response<T>): T? {

        var title = "Error"
        var internal_errorCode = EXP_DEF_CODE
        var errorBody: String = SOMETHINGWRONG
        var expType = "connection"

        try {
            val response = call.invoke()
            response?.let {
                if (response.isSuccessful) {
                    response?.body()?.let { body ->
                        return body
                    }
                    return null
                } else {

                    val error = response.errorBody()?.string()

                    try {
                        errorBody = onError(error).message

                        internal_errorCode = onError(error).cod.toInt()
                    } catch (ex: Exception) {

                        expType = "def"

                        throw ApiException(SOMETHINGWRONG)
                    }

                    expType = "api"
                   throw ApiErrors(
                       title,
                      errorBody,

                      internal_errorCode
                  )

                }
            }
        } catch (ex: Throwable) {
            when (expType) {
                "def" -> {
                    throw ApiException(SOMETHINGWRONG)
                }

                "api" -> {
                    throw ApiErrors(title, errorBody, internal_errorCode)
                }
                else ->{
                    throw NoInternetException(CONNECTION_ERROR)
                }

            }


        }
        return null

    }

    private fun onError(error: String?): Error_Model_1 {
        val g_parse = Gson()
        return g_parse.fromJson(error, Error_Model_1::class.java)


    }

}