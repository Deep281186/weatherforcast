package com.sportsparty.app.factory

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.weatherforcast.android.home.fragments.ui.forcast.ForcastViewModel
import com.weatherforcast.android.home.fragments.ui.home.HomeViewModel
import com.weatherforcast.android.webUtils.Repository


@Suppress("UNCHECKED_CAST")
class ViewModel_factory(
    private val repository: Repository

) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(
                repository
            ) as T

        }else if (modelClass.isAssignableFrom(ForcastViewModel::class.java)) {
            return ForcastViewModel(
                repository
            ) as T
        } else {
            return throw IllegalArgumentException("ViewModel Not Found")
        }


    }


}