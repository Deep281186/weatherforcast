package com.weatherforcast.android.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.weatherforcast.android.R

fun Context.progressDialog(): Dialog {
    var dialog = Dialog(this)
    // .dialog = dialog
    dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    var lp = dialog.getWindow()?.getAttributes();

    dialog.window!!.attributes.dimAmount = 0.0f
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

    dialog.setContentView(R.layout.progress_dialog)
    dialog?.window?.setLayout(
        WindowManager.LayoutParams.MATCH_PARENT,
        WindowManager.LayoutParams.MATCH_PARENT
    );
    Handler().postDelayed(Runnable {
        if (dialog.isShowing) {
            dialog.setCancelable(true)
        }
    }, 4000)
    dialog.setCancelable(false)


    return dialog
}

fun Context.hideKeyboard(view: View) {
    val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}