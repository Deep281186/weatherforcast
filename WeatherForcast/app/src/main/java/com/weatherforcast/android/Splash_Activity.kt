package com.weatherforcast.android

import android.animation.Animator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.weatherforcast.android.home.NewHomeActivity
import com.weatherforcast.android.webUtils.Coroutines
import kotlinx.android.synthetic.main.activity_splash_.*
import kotlinx.coroutines.delay

class Splash_Activity : AppCompatActivity(), Animator.AnimatorListener {
    override fun onAnimationRepeat(p0: Animator?) {
    }

    override fun onAnimationEnd(p0: Animator?) {

    }

    override fun onAnimationCancel(p0: Animator?) {
    }

    override fun onAnimationStart(p0: Animator?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_)
        animationHandler()
        Coroutines.onMainthread {
            delay(4000)
            var i = Intent(this, NewHomeActivity::class.java)
            startActivity(i)
            finish()
        }
    }

    private fun animationHandler() {
        img_cloud.animate().alpha(1f).setStartDelay(500).setDuration(1000).start()
        img_sun.animate().alpha(1f).setStartDelay(500).setDuration(1000).start()
        img_cloud.animate().translationX(0f).setStartDelay(800).setDuration(3000).start()
        img_sun.animate().setListener(this).translationX(0f).setStartDelay(800).setDuration(3000)
            .start()
    }
}
